const { ApplicationCommandOptionType, PermissionFlagsBits } = require('discord.js')
const { RoleReactionBuilder } = require('../../database/models/roleReactionBuilder')
const { logger } = require('../../services/logger')

const pino = logger.child({ module: 'addrole' })
const addRoleReaction = {
    name: 'addrolereaction',
    description: 'Add a role and reaction to the reaction-role message',
    adminsonly: true,
    testonly: false,
    deleted: false,
    options: [
        {
            name: 'role',
            type: ApplicationCommandOptionType.Role,
            description: 'The role to add',
            required: true,
        },
        {
            name: 'emoji',
            type: ApplicationCommandOptionType.String,
            description: 'The emoji for the reaction',
            required: true,
        },
    ],
    permissionsRequired: [PermissionFlagsBits.ManageChannels, PermissionFlagsBits.ManageGuild, PermissionFlagsBits.ManageRoles],
    botpermissionsRequired: [PermissionFlagsBits.ManageRoles, PermissionFlagsBits.ManageChannels, PermissionFlagsBits.ManageMessages],
    callback: async (client, interaction) => {
        const guildId = interaction.guild.id
        const roleId = interaction.options.getRole('role').id
        const emoji = interaction.options.getString('emoji')
        const userId = interaction.user.id

        const roleReactionBuilder = await RoleReactionBuilder.findOne({ guildId, userId })
        if (!roleReactionBuilder)
            return interaction.reply({ content: 'No message found to add roles and reactions to. Use `/createmessage` to create a new message.', ephemeral: true })

        if (!Array.isArray(roleReactionBuilder.roleReactions))
            roleReactionBuilder.roleReactions = []

        roleReactionBuilder.roleReactions.push({ roleId, emoji })
        await roleReactionBuilder.markModified('roleReactions')

        await roleReactionBuilder.save()

        interaction.reply({ content: 'Role and reaction added. Use `/finalizemessage` when you\'re done adding roles and reactions.', ephemeral: true })
    },
}

module.exports = addRoleReaction
