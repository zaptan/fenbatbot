const { ApplicationCommandOptionType, PermissionFlagsBits } = require('discord.js')
const { RoleReaction } = require('../../database/models/roleReaction')

const removeReactionRole = {
    name: 'removereactionrole',
    description: 'Remove a reaction-role message and its associated data',
    adminsonly: true,
    testonly: false,
    deleted: false,
    options: [
        {
            name: 'message_id',
            type: ApplicationCommandOptionType.String,
            description: 'The ID of the reaction-role message to remove',
            required: true,
        },
    ],
    permissionsRequired: [PermissionFlagsBits.ManageChannels, PermissionFlagsBits.ManageGuild, PermissionFlagsBits.ManageRoles],
    botpermissionsRequired: [PermissionFlagsBits.ManageRoles, PermissionFlagsBits.ManageMessages],
    callback: async (client, interaction) => {
        const messageId = interaction.options.getString('message_id')

        const roleReaction = await RoleReaction.findOne({ messageId })
        if (!roleReaction)
            return interaction.reply({ content: 'No reaction-role message found with the provided ID.', ephemeral: true })


        await RoleReaction.deleteMany({ messageId })

        try {
            const message = await interaction.channel.messages.fetch(messageId)
            await message.delete()
        } catch (error) {
            console.error('Error deleting message:', error)
        }

        interaction.reply({ content: 'The reaction-role message and its associated data have been removed.', ephemeral: true })
    },
}

module.exports = removeReactionRole