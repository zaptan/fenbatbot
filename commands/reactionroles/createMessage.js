const { ApplicationCommandOptionType, PermissionFlagsBits } = require('discord.js')
const mongoose = require('mongoose')
const { RoleReactionBuilder } = require('../../database/models/roleReactionBuilder')

const createMessage = {
    name: 'createmessage',
    description: 'Start the process of creating a new reaction-role message',
    adminsonly: true,
    testonly: false,
    deleted: false,
    options: [
        {
            name: 'content',
            type: ApplicationCommandOptionType.String,
            description: 'The content of the reaction-role message',
            required: true,
        },
    ],
    permissionsRequired: [PermissionFlagsBits.ManageChannels, PermissionFlagsBits.ManageGuild, PermissionFlagsBits.ManageRoles],
    botpermissionsRequired: [PermissionFlagsBits.ManageRoles, PermissionFlagsBits.ManageChannels, PermissionFlagsBits.ManageMessages],
    callback: async (client, interaction) => {
        const content = interaction.options.getString('content')
        const guildId = interaction.guild.id
        const userId = interaction.user.id

        const roleReactionBuilder = new RoleReactionBuilder({
            _id: new mongoose.Types.ObjectId(),
            userId: userId,
            guildId: guildId,
            messageText: content,
            roles: [],
        })
        await roleReactionBuilder.save()

        interaction.reply({ content: 'The message has been created. Use `/addrolereaction` to add roles and reactions.', ephemeral: true })
    },
}

module.exports = createMessage

