const { PermissionFlagsBits } = require('discord.js')
const { EmbedBuilder } = require('discord.js')
const { RoleReaction } = require('../../database/models/roleReaction')

const listReactionRole = {
    name: 'listreactionrole',
    description: 'List all reaction-role messages for the current guild',
    adminsonly: true,
    testonly: false,
    deleted: false,
    options: [],
    permissionsRequired: [PermissionFlagsBits.ManageChannels, PermissionFlagsBits.ManageGuild, PermissionFlagsBits.ManageRoles],
    botpermissionsRequired: [PermissionFlagsBits.ManageRoles, PermissionFlagsBits.ManageChannels],
    callback: async (client, interaction) => {
        const guildId = interaction.guild.id

        const roleReactions = await RoleReaction.find({ guildId })

        const reactionRoleMap = new Map()

        roleReactions.forEach((roleReaction) => {
            const emoji = roleReaction.emoji
            const roleId = roleReaction.roleId
            const role = interaction.guild.roles.cache.get(roleId)
            const roleName = role ? role.name : 'Role not found'

            if (reactionRoleMap.has(roleReaction.messageId))
                reactionRoleMap.set(roleReaction.messageId, [
                    ...reactionRoleMap.get(roleReaction.messageId),
                    { emoji, roleName },
                ])
            else
                reactionRoleMap.set(roleReaction.messageId, [{ emoji, roleName }])

        })

        const embed = new EmbedBuilder()
            .setTitle('Reaction-Role Messages')
            .setDescription('List of all reaction-role messages and their associated roles and emojis.')

        reactionRoleMap.forEach((reactionRoles, messageId) => {
            const fieldValue = reactionRoles
                .map((reactionRole) => `${reactionRole.emoji} / ${reactionRole.roleName}`)
                .join(' | ')
            embed.addFields({ name: messageId, value: fieldValue })
        })

        interaction.reply({ embeds: [embed], ephemeral: true })
    },
}

module.exports = listReactionRole