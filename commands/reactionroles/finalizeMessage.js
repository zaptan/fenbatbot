const { EmbedBuilder, PermissionFlagsBits } = require('discord.js')
const Guild = require('../../database/models/guild')
const { RoleReaction } = require('../../database/models/roleReaction')
const { RoleReactionBuilder } = require('../../database/models/roleReactionBuilder')

const finalizeMessage = {
    name: 'finalizemessage',
    description: 'Finalize the reaction-role message and post it to the channel',
    adminsonly: true,
    testonly: false,
    deleted: false,
    options: [],
    permissionsRequired: [PermissionFlagsBits.ManageChannels, PermissionFlagsBits.ManageGuild, PermissionFlagsBits.ManageRoles],
    botpermissionsRequired: [PermissionFlagsBits.ManageRoles, PermissionFlagsBits.ManageChannels, PermissionFlagsBits.ManageMessages],
    callback: async (client, interaction) => {
        const guildId = interaction.guild.id

        // Fetch the guild document
        const guildData = await Guild.findOne({ guildid: guildId })
        if (!guildData || !guildData.botchannel)
            return interaction.reply({ content: 'No botchannel found in the guild document. Make sure it is set correctly.', ephemeral: true })

        const botChannel = interaction.guild.channels.cache.get(guildData.botchannel)
        if (!botChannel)
            return interaction.reply({ content: 'No botchannel found in the current guild. Make sure it is set correctly.', ephemeral: true })

        const roleReactionBuilder = await RoleReactionBuilder.findOne({ guildId })
        if (!roleReactionBuilder)
            return interaction.reply({ content: 'No message found to finalize. Use `/createmessage` to create a new message.', ephemeral: true })


        const embed = new EmbedBuilder()
            .setDescription(roleReactionBuilder.messageText)
            .setFooter({ text: 'React to this message to receive a role' })

        const message = await botChannel.send({ embeds: [embed] })

        for (const role of roleReactionBuilder.roleReactions) {
            await message.react(role.emoji)
            const roleReaction = new RoleReaction({ messageId: message.id, guildId, roleId: role.roleId, emoji: role.emoji })
            await roleReaction.save()
        }

        await RoleReactionBuilder.deleteOne({ guildId })

        interaction.reply({ content: 'The reaction-role message has been posted and saved.', ephemeral: true })
    },
}

module.exports = finalizeMessage
