module.exports = {
    name: 'ping',
    description: 'pong!',
    adminsonly: false,
    testonly: false,

    callback: async (client, interaction) => {
        await interaction.deferReply()
        const reply = await interaction.fetchReply()
        const ping = reply.createdTimestamp - interaction.createdTimestamp
        interaction.editReply(`🏓  Pong! Client ${ping}ms ; web ${client.ws.ping}ms`)
    },
}