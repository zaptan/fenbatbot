const { Client, Interaction, ApplicationCommandOptionType } = require('discord.js')
const { Types } = require('mongoose')
const dayjs = require('dayjs')
const customParseFormat = require('dayjs/plugin/customParseFormat')
dayjs.extend(customParseFormat)

const Etags = require('../../database/models/e621tags')
const { logger } = require('../../services/logger')

const pino = logger.child({ module: 'tags' })
const pickuser = async (user) => {
    let person = {}
    if (user) {
        person = await Etags.findOne({ userid: user.id })
        return person
    }
    return { error: 'not Found' }
}


module.exports = {
    name: 'tags',
    description: 'show your e621 tags',
    adminsonly: false,
    testonly: false,

    /**
   *
   * @param {Client} client
   * @param {Interaction} interaction
   */
    callback: async (client, interaction) => {
        await interaction.deferReply({ ephemeral: true })
        if (interaction.options.get('taglist')) {
            const tags = interaction.options.get('taglist').value.split(' ')

            let guy = await Etags.findOne({ userid: interaction.member.user.id })
            if (!guy) {
                guy = await new Etags({
                    _id: new Types.ObjectId(),
                    userid: interaction.member.user.id,
                    username: interaction.member.user.name,
                    tags: ['-female'],
                    lastupdate: Date.now(),
                    weight: 1,
                })
                await guy.save().catch((err) => pino.error(err))
            } else {
                pino.info(`${guy.username} updated tags: ${tags.join(' ')}`)
                guy.tags = tags
                await guy.save().catch((err) => pino.error(err))
            }
            interaction.editReply(`Your tags are updated to: ${tags.join(' ')}`)
            return
        }

        const target = await pickuser(interaction.member.user)
        interaction.editReply(target.tags.join(' '))
    },
    options: [
        {
            name: 'taglist',
            description: 'list of tags to replace your current tags with',
            type: ApplicationCommandOptionType.String,
            required: false,
        },
    ],
}