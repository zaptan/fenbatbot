const { ApplicationCommandOptionType, PermissionFlagsBits } = require('discord.js')
const Guild = require('../../database/models/guild')
const mongoose = require('mongoose')

const updateGuild = {
    name: 'updateguild',
    description: 'Update guild settings',
    adminsonly: true,
    testonly: false,
    deleted: false,
    options: [
        {
            name: 'muterole',
            type: ApplicationCommandOptionType.Role,
            description: 'The role used for muting',
            required: false,
        },
        {
            name: 'memberrole',
            type: ApplicationCommandOptionType.Role,
            description: 'The role assigned to new members',
            required: false,
        },
        {
            name: 'botchannel',
            type: ApplicationCommandOptionType.Channel,
            description: 'The channel dedicated to bot commands',
            required: false,
        },
        {
            name: 'nsfwchannel',
            type: ApplicationCommandOptionType.Channel,
            description: 'The channel dedicated to NSFW content',
            required: false,
        },
        {
            name: 'e621posts',
            type: ApplicationCommandOptionType.Boolean,
            description: 'Whether e621 posts are allowed (true) or not (false)',
            required: false,
        },
    ],
    permissionsRequired: [PermissionFlagsBits.ManageChannels, PermissionFlagsBits.ManageGuild, PermissionFlagsBits.ManageRoles],
    botpermissionsRequired: [PermissionFlagsBits.ManageRoles, PermissionFlagsBits.ManageChannels],
    callback: async (client, interaction) => {
        const guildId = interaction.guild.id

        const hasOptions =
          interaction.options.getRole('muterole') ||
          interaction.options.getRole('memberrole') ||
          interaction.options.getChannel('botchannel') ||
          interaction.options.getChannel('nsfwchannel') ||
          interaction.options.getBoolean('e621posts') !== null

        let guildData = await Guild.findOne({ guildid: guildId })

        if (!guildData) {
            guildData = new Guild({
                _id: new mongoose.Types.ObjectId(),
                guildid: guildId,
                lastupdate: new Date(),
                bot: client.user.id,
            })
            await guildData.save()
        }

        if (!hasOptions) {
            const formatDate = new Intl.DateTimeFormat('en-US', { dateStyle: 'medium', timeStyle: 'medium' }).format(guildData.lastupdate)
            const dataText = `
    Mute Role ID: ${guildData.muteRoleID || 'Not set'}
    Member Role ID: ${guildData.memberRoleID || 'Not set'}
    Bot Channel: ${guildData.botchannel || 'Not set'}
    NSFW Channel: ${guildData.nsfwchannel || 'Not set'}
    e621 Posts: ${guildData.e621posts}
    Last Update: ${formatDate}
    `

            return interaction.reply({ content: `Current guild settings:\n\`\`\`${dataText}\`\`\``, ephemeral: true })
        }

        const updatedData = {
            lastupdate: new Date(),
        }

        if (interaction.options.getRole('muterole'))
            updatedData.muteRoleID = interaction.options.getRole('muterole').id


        if (interaction.options.getRole('memberrole'))
            updatedData.memberRoleID = interaction.options.getRole('memberrole').id


        if (interaction.options.getChannel('botchannel'))
            updatedData.botchannel = interaction.options.getChannel('botchannel').id


        if (interaction.options.getChannel('nsfwchannel'))
            updatedData.nsfwchannel = interaction.options.getChannel('nsfwchannel').id


        if (interaction.options.getBoolean('e621posts') !== null)
            updatedData.e621posts = interaction.options.getBoolean('e621posts')


        await Guild.updateOne({ guildid: guildId }, { $set: updatedData })

        interaction.reply({ content: 'Guild settings have been updated.', ephemeral: true })
    },
}

module.exports = updateGuild
