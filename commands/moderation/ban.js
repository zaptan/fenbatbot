const { ApplicationCommandOptionType, PermissionFlagsBits } = require('discord.js')

module.exports = {
    name: 'ban',
    description: 'bans everyone forever',
    adminsonly: true,
    testonly: false,
    deleted: true,
    options: [
        {
            name: 'target-user',
            description: 'user to ban',
            required: true,
            type: ApplicationCommandOptionType.Mentionable,
        },
        {
            name: 'reason',
            description: 'reason for ban',
            type: ApplicationCommandOptionType.String,
        },
    ],
    permissionsRequired: [PermissionFlagsBits.BanMembers, PermissionFlagsBits.ManageRoles],
    botpermissionsRequired: [PermissionFlagsBits.BanMembers],

    callback: (client, interaction) => {
        interaction.reply('banned')
    },
}