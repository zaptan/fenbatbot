const { ApplicationCommandOptionType, PermissionFlagsBits } = require('discord.js')
const setclock = require('../../utils/clock')
const Guild = require('../../database/models/guild')
const { logger } = require('../../services/logger')

const pino = logger.child({ module: 'post' })

const updatePostRate = {
    name: 'post',
    description: 'Update the post rate setting',
    adminsonly: false,
    testonly: false,
    deleted: false,
    options: [
        {
            name: 'rate',
            type: ApplicationCommandOptionType.Integer,
            description: 'The post rate (0 to 6)',
            required: true,
        },
    ],
    permissionsRequired: [PermissionFlagsBits.ManageChannels, PermissionFlagsBits.ManageGuild, PermissionFlagsBits.ManageRoles],
    botpermissionsRequired: [],
    callback: async (client, interaction) => {
        const guildId = interaction.guild.id
        const rate = interaction.options.getInteger('rate')

        if (rate < 0 || rate > 6)
            return interaction.reply({ content: 'Please provide a valid rate between 0 and 6.', ephemeral: true })


        const updatedData = {
            rate: rate,
            e621posts: rate !== 0,
        }
        pino.warn({ guildId, updatedData })
        await Guild.updateOne({ guildid: guildId }, { $set: updatedData })
        const times = setclock(rate)
        interaction.reply({ content: `Post rate has been changed to ${times}`, ephemeral: true })
    },
}

module.exports = updatePostRate
