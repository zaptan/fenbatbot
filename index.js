require('dotenv').config()
const loggerobj = require('./services/logger')
const { Client, Collection, IntentsBitField, Partials } = require('discord.js')
const mongoose = require('./database/db')
const Guilds = require('./database/models/guild')
const fs = require('fs')
const dayjs = require('dayjs')
const e621 = require('./services/e621')
const embed = require('./services/embed')
const eventHandler = require('./handlers/eventHandler')

let _token = process.env.DISCORD_PROD_TOKEN
let _posttimes = [4, 10, 16, 22]
let _inc = 'H'
let _len = 10000

if (process.env.DEVELOPMENT == 'true') {
    _token = process.env.DISCORD_DEVL_TOKEN
    _posttimes = [0, 20, 40]
    _inc = 's'
    _len = 100
    loggerobj.logger.level = 'debug'
}

// init database
mongoose.init()
// setup logger
const pino = loggerobj.logger.child({ module: 'index' })

// setup bot permissions
const client = new Client({
    intents: [
        IntentsBitField.Flags.Guilds,
        IntentsBitField.Flags.GuildIntegrations,
        IntentsBitField.Flags.GuildMessageReactions,
        IntentsBitField.Flags.GuildEmojisAndStickers,
    ],
    partials: [Partials.Message, Partials.Reaction, Partials.Channel],
})

eventHandler(client)

// setup timed events on the hour
let last = dayjs()
setInterval(() => {
    if (last.format(_inc) != dayjs().format(_inc)) {
        last = dayjs()
        client.emit('timer', parseInt(dayjs().format(_inc)))
    }
}, _len)

client.login(_token)
