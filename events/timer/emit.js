const setclock = require('../../utils/clock')
const Guild = require('../../database/models/guild')
const { random } = require('../../services/e621.js')
const embed = require('../../services/embed.js')
const { logger } = require('../../services/logger')

const pino = logger.child({ module: 'timer' })

async function sendMessageToBotChannel(client, guildId) {
    const guildData = await Guild.findOne({ guildid: guildId })

    if (!guildData || !guildData.nsfwchannel) return

    const guild = client.guilds.cache.get(guildId)
    const nsfwchannel = guild.channels.cache.get(guildData.nsfwchannel)

    if (!nsfwchannel) return

    // Customize this message as needed

    const post = embed(await random())
    nsfwchannel.send({ embeds: [post] })
}


module.exports = async (client, hour) => {
    const guilds = client.guilds.cache

    guilds.forEach(async (guild) => {
        const guildData = await Guild.findOne({ guildid: guild.id })

        if (!guildData || !guildData.rate || !guildData.e621posts)
            return

        const postTimes = setclock(guildData.rate)

        if (postTimes.includes(hour))
            await sendMessageToBotChannel(client, guild.id)
    })
}