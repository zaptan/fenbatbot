const areCommandsDifferent = require('../../utils/areCommandsDifferent')
const getApplicationCommands = require('../../utils/getApplicationCommands')
const getLocalCommands = require('../../utils/getLocalCommands')
const { logger } = require('../../services/logger')

const pino = logger.child({ module: 'register' })
module.exports = async (client) => {

    try {
        const localCommands = getLocalCommands()
        for (const guild of client.guilds.cache.values()) {
            const guildId = guild.id
            const applicationCommands = await getApplicationCommands(client, guildId)

            for (const localCommand of localCommands) {
                const { name, description, options } = localCommand

                const existingCommand = await applicationCommands.cache.find(cmd => cmd.name === name)

                if (existingCommand) {
                    if (localCommand.deleted) {
                        await applicationCommands.delete(existingCommand.id)
                        pino.warn(`🗑️  ${name} 🗑️ @ ${guild.name}`)
                        continue
                    }
                    if (areCommandsDifferent(existingCommand, localCommand)) {
                        await applicationCommands.edit(existingCommand.id, { description, options })
                        pino.warn(`🖊️  edited: ${name} @ ${guild.name}`)
                    }
                } else {
                    if (localCommand.deleted) {
                        pino.warn(`➡️  skipping ${name} @ ${guild.name}`)
                        continue
                    }
                    await applicationCommands.create({ name, description, options })
                    pino.warn(`✔️  registered command: ${name} @ ${guild.name}`)
                }
            }
        }
    } catch (error) {
        pino.warn(`❌ error: ${error}`)

    }
}