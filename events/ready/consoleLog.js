const { logger } = require('../../services/logger')

const pino = logger.child({ module: 'ready' })
module.exports = client => {

    pino.warn(`${client.user.tag} is online`)
}