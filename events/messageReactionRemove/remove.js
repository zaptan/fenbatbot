const { Client, Message } = require('discord.js')
const { RoleReaction } = require('../../database/models/roleReaction')
const { logger } = require('../../services/logger')

const pino = logger.child({ module: 'MRR' })

/**
 *
 * @param {Client} client
 * @param {Message} reaction
 */
async function handleReactionRemove(client, reaction, user) {
    if (user.bot) return
    if (reaction.partial)
        try {
            await reaction.fetch()
        } catch (error) {
            pino.error('Error fetching partial reaction: ' + error)
            return
        }

    const roleReaction = await RoleReaction.findOne({ messageId: reaction.message.id, emoji: reaction.emoji.name })
    if (!roleReaction) return

    const guild = client.guilds.cache.get(reaction.message.guildId)
    const member = await guild.members.fetch(user.id)
    const role = guild.roles.cache.get(roleReaction.roleId)

    await member.roles.remove(role)
}

module.exports = handleReactionRemove