const getLocalCommands = require('../../utils/getLocalCommands')
const { logger } = require('../../services/logger')

const pino = logger.child({ module: 'handleCommands' })
module.exports = async (client, interaction) => {
    if (!interaction.isChatInputCommand()) return

    const localCommands = getLocalCommands()

    try {
        const commandObject = localCommands.find(cmd => cmd.name === interaction.commandName)
        if (!commandObject) return
        if (commandObject.permissionsRequired?.length)
            for (const permission of commandObject.permissionsRequired)
                if (!interaction.member.permissions.has(permission)) {
                    interaction.reply({
                        content: 'sorry you can not run this command.',
                        ephemeral: true,
                    })
                    return
                }
        if (commandObject.botpermissionsRequired?.length)
            for (const permission of commandObject.botpermissionsRequired) {
                const bot = interaction.guild.members.me
                if (!bot.permissions.has(permission)) {
                    interaction.reply({
                        content: 'sorry I can not run this command.',
                        ephemeral: true,
                    })
                    return
                }
            }
        await commandObject.callback(client, interaction)

    } catch (error) {
        pino.error(`there was an error: ${error}`)
    }
}