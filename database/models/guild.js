const mongoose = require('mongoose')
const guildSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    guildid: String,
    lastupdate: Date,
    prefix: { type: String, default: '!' },
    muteRoleID: { type: String, required: false },
    memberRoleID: { type: String, required: false },
    botchannel: { type: String, required: false },
    nsfwchannel: { type: String, required: false },
    e621posts: { type: Boolean, required: false },
    rate: { type: Number, required: false },
    bot: String,
})

module.exports = new mongoose.model('guild', guildSchema, 'guilds')
