const mongoose = require('mongoose')
const subSchema = new mongoose.Schema({
    user: { type: String, required: false },
    date: Date,
    event: { type: String, required: false },
})

const postSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    postid: String,
    img: { type: String, required: false },
    thumb: { type: String, required: false },
    dates: [subSchema],
})

module.exports = new mongoose.model('posts', postSchema, 'posts')
