const mongoose = require('mongoose')

const roleReactionBuilderSchema = new mongoose.Schema({
    userId: { type: String, required: true },
    guildId: { type: String, required: true },
    messageText: { type: String, required: true },
    roleReactions: [
        {
            emoji: { type: String, required: true },
            roleId: { type: String, required: true },
        },
    ],
})

const RoleReactionBuilder = mongoose.model('RoleReactionBuilder', roleReactionBuilderSchema)

module.exports = { RoleReactionBuilder }
