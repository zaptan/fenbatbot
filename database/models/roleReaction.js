const mongoose = require('mongoose')

const roleReactionSchema = new mongoose.Schema({
    messageId: { type: String, required: true },
    guildId: { type: String, required: true },
    emoji: { type: String, required: true },
    roleId: { type: String, required: true },
})

const RoleReaction = mongoose.model('RoleReaction', roleReactionSchema)

module.exports = { RoleReaction }