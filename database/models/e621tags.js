const mongoose = require('mongoose')
const guildSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    userid: String,
    username: String,
    tags: [{ type: String }],
    lastupdate: Date,
    weight: Number,
})

module.exports = new mongoose.model('e621tags', guildSchema, 'e621tags')
