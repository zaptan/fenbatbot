require('dotenv').config()
const logger = require('../services/logger')
const mongoose = require('mongoose')

const pino = logger.logger.child({ 'module':'database' })

module.exports = {
    init: () => {
        mongoose.connect(
            `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_TOKEN}@${process.env.MONGO_HOST}`,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                autoIndex: false,
                connectTimeoutMS: 10000,
                family: 4,
            },
        )
        mongoose.connection.on('connected', () => {
            pino.debug('connected to Mongo')
        })
        mongoose.connection.on('disconnected', () => {
            pino.debug('disconnected from Mongo')
        })
        mongoose.connection.on('err', (err) => {
            pino.error(err)
        })
    },
    close: () => {mongoose.connection.close()},
}
