module.exports = (times) => {
    const utcoffset = 8
    const start = 20 + utcoffset

    const rate = Math.round(24.0 / times)
    const arr = []
    for (let index = 0; index < times; index++) {
        let boop = rate * index + start
        if (boop >= 24) boop -= 24
        arr.push(boop)
    }
    arr.sort((a, b) => a - b)
    return arr
}