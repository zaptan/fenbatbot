const { EmbedBuilder } = require('discord.js')
const logger = require('./logger')
const colors = require('./colors')
const pino = logger.logger.child({ 'module': 'embed' })

module.exports = (e621result) => {
    pino.debug(e621result)
    // console.dir(e621result)
    const emb = new EmbedBuilder()
    emb.setTitle(e621result.id.toString())
    emb.setImage(e621result.url)
    emb.setURL(e621result.link)
    emb.setDescription(e621result.jerk.tags.join(', '))
    emb.setColor(colors.findColor(e621result.jerk.username))
    return emb
}