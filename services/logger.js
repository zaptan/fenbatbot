const pino = require('pino')
const dayjs = require('dayjs')
const transport = pino.transport({
    targets: [
        {
            level: 'debug',
            target: 'pino-pretty',
            options: {
                translateTime: 'SYS:HH:MM:ss',
                ignore: 'pid',
                colorize: true,
                singleLine: true,
            },
        },
        {
            level: 'info',
            target: 'pino/file',
            options: { destination: `./logs/${dayjs().format('YYYYMMDD')}_log.txt` },
        },
    ],
})
const actsport = pino.transport({
    targets: [
        {
            level: 'info',
            target: 'pino/file',
            options: { destination: `./logs/${dayjs().format('YYYYMMDD')}_actions.txt` },
        },
    ],
})

const logger = pino(transport)
logger.level = 'trace'
const actions = pino(actsport)
actions.level = 'trace'

module.exports = { logger, actions }
