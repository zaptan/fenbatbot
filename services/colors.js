const cc = require('color')
const logger = require('./logger')
const pino = logger.logger.child({ 'module': 'color' })

const arrColors = [
    cc('#ff6188'),
    cc('#fc9867'),
    cc('#ffd866'),
    cc('#a9dc76'),
    cc('#78dce8'),
    cc('#0094FF'),
    cc('#ae81ff'),
    cc('#ffc0c0'),
]

module.exports = {
    background: cc('#263238'),
    foreground: cc('#ffffff'),
    selBackground: cc('#80cbc4'),
    pink: arrColors[0],
    green: arrColors[1],
    cyan: arrColors[2],
    orange: arrColors[3],
    purple: arrColors[4],
    blue: arrColors[5],
    silver: arrColors[6],
    findColor: (name) => {
        const code = name.split('').map(char => char.charCodeAt(0)).reduce((a, b) => a + b)

        // pino.info(`${code}, ${code % arrColors.length}`)
        return arrColors[code % arrColors.length].hex()
    },
}
