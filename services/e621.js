require('dotenv').config()
const logger = require('./logger')
const { Types } = require('mongoose')
const weighted = require('weighted')
const dayjs = require('dayjs')
const customParseFormat = require('dayjs/plugin/customParseFormat')
dayjs.extend(customParseFormat)
const Etags = require('../database/models/e621tags')
const Posts = require('../database/models/posts')

const pino = logger.logger.child({ module: 'e621' })

const E621 = require('e621')
const e621 = new E621()

const blacklisttags = ['-scat', '-gore', '-cub', '-shota', '-lobotomy', '-webm', '-animated']
const _random = (max) => Math.floor(Math.random() * (max + 1))
const _url = (id) => `https://e621.net/posts/${id}`
const pickuser = async (user) => {
    let person = {}
    if (user) {
        person = await Etags.findOne({ userid: user.id })
        person.ptype = 'post'
        return person
    }
    const profile = await Etags.find()
    const list = []
    const listw = []
    profile.forEach((p) => {
        p.weight = p.weight || 1
        list.push(p.userid)
        listw.push(p.weight)
    })
    const pick = weighted.select(list, listw)
    const others = await Etags.find({ userid: { $not: { $eq: pick } } })
    await Etags.updateOne({ userid: pick }, { weight: 1 })
    person = await Etags.findOne({ userid: pick })
    others.forEach(async (p) => {
        await Etags.updateOne({ userid: p.userid }, { $inc: { weight: 1 } })
    })
    person.ptype = 'rand'
    return person
}
const pickpost = async (person) => {
    // pino.debug(person)
    const list = await e621.posts.search({ tags: blacklisttags.concat(person.tags), limit: 50 })
    let i = 0
    let final = {}
    let exit = false
    do {
        i++
        const r = _random(list.length - 1)
        if (process.env.TRACE) pino.debug(list[r])
        final = {
            id: list[r].id,
            link: _url(list[r].id),
            url: list[r].file.url,
            preview: list[r].preview.url,
            tags: list[r].tags,
            jerk: person,
        }
        let history = await Posts.findOne({ postid: final.id })
        if (!history) {
            history = await new Posts({
                _id: new Types.ObjectId(),
                postid: final.id,
                img: final.url,
                thumb: final.preview,
                dates: [{ user: person.username, date: Date.now(), event: 'rand' }],
            })
            await history.save().catch((err) => pino.error(err))
            exit = true
        } else {
            history.dates.push({
                user: person.username,
                date: Date.now(),
                event: 'rand',
            })
            await history.save().catch((err) => pino.error(err))
        }
        if (i > 10) exit = true
    } while (!exit)
    pino.debug('final', final)
    return final
}

module.exports = {
    link: (id) => `https://e621.net/posts/${id}`,
    random: async () => await pickpost(await pickuser()),
}
